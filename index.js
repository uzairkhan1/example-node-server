
let express = require('express');
let bodyParser = require('body-parser');
let http = require('http');
let cors = require('cors');
let path = require('path');
let io = require('socket.io');
let dotenv = require('dotenv');
dotenv.config();

let { SetChatConfig, StartChat } = require('node-chat-stream').NodeChatSteam;

const app = express();
const httpServer = http.Server(app);
const port = process.env.PORT || 5000;
const socket = io(httpServer); //integrating socketio
const __dirname_ = path.resolve();
//set the express.static middleware
app.use(express.static(__dirname_ + '/public/'));
//bodyparser middleware
app.use(bodyParser.json());
app.use(cors()); //cors


SetChatConfig({ db_url: process.env.DB_URL });
StartChat(socket, app);

httpServer.listen(port, () => {
  console.log("Running on Port: " + port);
});
